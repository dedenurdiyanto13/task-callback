$.ajax({
    url: 'https://jsonplaceholder.typicode.com/users',
    success: result => {
        // console.log(result);
        const employes = result;
        let tables = `<thead>
                        <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Username</th>
                        <th scope="col">Email</th>
                        <th scope="col">Address</th>
                        <th scope="col">Company</th>
                        </tr>
                    </thead>`;
        employes.forEach(employe => {
            tables += `<tbody>
                            <tr>
                            <th scope="row">${employe.id}</th>
                            <td>${employe.name}</td>
                            <td>${employe.username}</td>
                            <td>${employe.email}</td>
                            <td>
                                ${employe.address['street']},
                                ${employe.address['suite']},
                                ${employe.address['city']}
                            </td>
                            <td>${employe.company['name']}</td>
                            </tr>
                        </tbody>`;
            });
        $('#emp_table').html(tables);
    },
    error: (e) => {
        console.log(e.responseText);
    }
});